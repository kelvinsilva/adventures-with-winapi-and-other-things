//created by katokato (kelvin silva)
//credits given to: http://www.dreamincode.net/code/snippet357.htm for the default webbrowser code
//credits also given to http://www.cplusplus.com/forum/general/4422/ for teh string to char conversion,
// dont worry, i didnt just copy the snippet, i actually understand it so its all good.
//credits to http://www.cplusplus.com/doc/tutorial/files/ for the input output reference
//credits given to http://www.cplusplus.com/reference/iostream/ios_base/flags/ for the hex formatting
#include <iostream>
#include <windows.h>
#include <string>
#include <fstream>
using namespace std;
void create();
void display();
void moreinfo();
int main()
{

    int truefalse = 1;
    while (truefalse)
    {
    int choice = 0;
    cout << "Enter choice" << endl;
    cout << "1. Create a messagebox\n";
    cout << "2. Display a messagebox\n";
    cout << "3. More info on messageboxes\n";
    cout << "Choice: "; cin >> choice;
    if (choice > 3)
    {
  cout << "that is not a choice, choose a choice from 1-3\n";
    }
    else
    {
truefalse = 0;
    switch (choice) {
    case 1 : create();
    break;
    case 2 : display();
    break;
    case 3 : moreinfo();
    break;
    }

}
    }
    return 0;
    }
void create()
{
    //since there is an enter stored, it just clear it, so it wont enter(yess literally the enter button) automatically the title of messagebox
    cin.get();
    string thetitle;
    cout << "Enter title of messagebox\n";
    //convert typed string to char so i can use it as variables for the messageboxes
   getline(cin, thetitle);
char *title=new char[thetitle.size()+1];
title[thetitle.size()]='\0';
memcpy(title,thetitle.c_str(),thetitle.size());
//does the same except for the message
string themessage;
    cout << "\nEnter the message of messagebox\n";
    getline(cin, themessage);
    char *message=new char[themessage.size()+1];
message[thetitle.size()]='0';
memcpy(message,themessage.c_str(),themessage.size());
const UINT type[8] = {0x002L, 0x006L, 0x00004000L, 0x00L, 0x001L, 0x005L, 0x004L, 0x003L};
    const UINT icons[8] = {0x0030L, 0x0030L, 0x0040L, 0x0040L, 0x0020L, 0x0010L, 0x0010L, 0x0010L};

cout << "Choose the type of messagebox that you want to create: \n";
cout << "1. MB_ABORTRETRYIGNORE, This messagebox contains abort, retry and ignore buttons";
cout << "2. MB_CANCELTRYCONTINUE, This messagebox contains cancel, try again, and continue buttons\n";
cout << "3. MB_HELP, This messagebox contains a help button, when it is clicked, the system sends a WM_HELP message\n";
cout << "4. MB_OK, This messagebox simply contains an OK button\n";
cout << "5. MB_OKCANCEL, This messagebox contains an OK, and cancel button\n";
cout << "6. MB_RETRYCANCEL, This messagebox contains retry and cancel buttons\n";
cout << "7. MB_YESNO, This messsagebox contains yes and no buttons\n";
cout << "8. MB_YESNOCANCEL, This messagebox contains a yes no and cancel buttons\n";
int choiceoftype = 0;
UINT whatuserchose;
cin >> choiceoftype;
if (choiceoftype > 8)
{
cout << choiceoftype << " is not a choice";
cout << "\n enter a choice: ";
cin >> choiceoftype;
}

switch (choiceoftype)
{
    case 1 : whatuserchose = type[0];
    break;
    case 2 : whatuserchose = type[1];
    break;
    case 3 : whatuserchose = type[2];
    break;
    case 4 : whatuserchose = type[3];
    break;
    case 5 : whatuserchose = type[4];
    break;
    case 6 : whatuserchose = type[5];
    break;
    case 7: whatuserchose = type[6];
    break;
    case 8 : whatuserchose = type[7];
    break;
}

cout << "Do you want an icon added to your messagebox?(y/n)\n";

int whatuserchoseicon = 0;
int whatwhatuserchoseicon = 0;
char yesno;
cin >> yesno;
if (yesno == 'y')
{
    cout << "Choose an icon\n";
    cout << "1. MB_ICONEXCLAMATION, This icon is an exclamation mark enclosed in a yellow triangle\n";
    cout << "2. MB_ICONWARNING, This icon is an exclamation mark enclosed in a yellow triangle\n";
    cout << "3. MB_ICONINFORMATION, This icon is a lowercase letter i enclosed in a circle\n";
    cout << "4. MB_ICONASTERISK, This icon is a lowercase letter i enclosed in a circle\n";
    cout << "5. MB_ICONQUESTION, This icon is a question mark\n";
    cout << "6. MB_ICONSTOP, This icon is a stop sign\n";
    cout << "7. MB_ICONERROR, Another stop sign\n";
    cout << "8. MB_ICONHAND, Yet another, stop sign\n";
    cin >> whatuserchoseicon;
    if (whatuserchoseicon > 8.)
    {
  cout << "that is not a choice\n";
  cout << "choose a choice 1-8\n";
  cin >> whatuserchoseicon;
    }
    switch (whatuserchoseicon){
    case 1 : whatwhatuserchoseicon = icons[0];
    break;
    case 2 : whatwhatuserchoseicon = icons[1];
    break;
    case 3 : whatwhatuserchoseicon = icons[2];
    break;
    case 4 :whatwhatuserchoseicon = icons [3];
    break;
    case 5 : whatwhatuserchoseicon = icons[4];
    break;
    case 6 : whatwhatuserchoseicon = icons[5];
    break;
    case 7 : whatwhatuserchoseicon = icons[6];
    break;
    case 8 : whatwhatuserchoseicon = icons[7];
    break;
    }
}
UINT ultimatechoice = 0;
ultimatechoice = whatuserchose + whatwhatuserchoseicon;
cout << "Do you want to see your messagebox? (y/n)";
char yn;
cin >> yn;
if (yn == 'y')
{
MessageBox(NULL, title, message, ultimatechoice);
}
    char that_choice = 0;
cout << "Would you like to save the code for this messagebox to a text file?\n (WARNING: the text file is kept on the same folder as this program(y/n)?\n";
cin >> that_choice;
if (that_choice == 'y')
{

  string thetitle11;
  cin.get();
    cout << "Enter filename\n";
    //convert typed string to char so i can use it as variables for the messageboxes
   getline(cin, thetitle11);
char *title11=new char[thetitle11.size()+1];
title11[thetitle11.size()]='\0';
memcpy(title11,thetitle11.c_str(),thetitle11.size());
    ofstream filein;
    filein.open (title11);
    filein.flags ( ios::right | ios::hex | ios::showbase );
    filein << "MessageBox(" << "NULL, " << "\"" << title <<"\"" << ", " <<"\"" << message <<"\""<<", " <<hex << ultimatechoice << ")";
    filein.close();
}

cout << "returning to main menu..\n";
system("cls");
int truefalse = 1;
    while (truefalse)
    {
    int choice = 0;
    cout << "Enter choice" << endl;
    cout << "1. Create a messagebox\n";
    cout << "2. Display a messagebox\n";
    cout << "3. More info on messageboxes\n";
    cout << "Choice: "; cin >> choice;
    if (choice > 3)
    {
  cout << "that is not a choice, choose a choice from 1-3\n";
    }
    else
    {
truefalse = 0;
    switch (choice) {
    case 1 : create();
    break;
    case 2 : display();
    break;
    case 3 : moreinfo();
    break;
    }

}
    }

}

void display()
{
    cin.get();
  string thetitle1;
    cout << "Enter title of messagebox\n";
    //convert typed string to char so i can use it as variables for the messageboxes
   getline(cin, thetitle1);
char *title1=new char[thetitle1.size()+1];
title1[thetitle1.size()]='\0';
memcpy(title1,thetitle1.c_str(),thetitle1.size());
//does the same except for the message
string themessage1;
    cout << "\nEnter the message of messagebox\n";
    getline(cin, themessage1);
    char *message1=new char[themessage1.size()+1];
message1[thetitle1.size()]='0';
memcpy(message1,themessage1.c_str(),themessage1.size());
cout << "Enter the UINT value: \n";
UINT thatvalues;
cin >> thatvalues;
cin.get();
cout << "Press enter to display messagebox.";
cin.get();

MessageBox(NULL, title1, message1, thatvalues);
cout << "Returning to main menu, press enter to continue:\n";
cin.get();
system("cls");
int truefalse = 1;
    while (truefalse)
    {
    int choice = 0;
    cout << "Enter choice" << endl;
    cout << "1. Create a messagebox\n";
    cout << "2. Display a messagebox\n";
    cout << "3. More info on messageboxes\n";
    cout << "Choice: "; cin >> choice;
    if (choice > 3)
    {
  cout << "that is not a choice, choose a choice from 1-3\n";
    }
    else
    {
truefalse = 0;
    switch (choice) {
    case 1 : create();
    break;
    case 2 : display();
    break;
    case 3 : moreinfo();
    break;
    }

}
    }
}
void moreinfo()
{
    ShellExecute(NULL, "open", "http://msdn.microsoft.com/en-us/library/ms645505%28VS.85%29.aspx",NULL, NULL, SW_SHOWNORMAL);
    cout << "Returning to main menu... Press enter to continue\n";

cin.get();
    cin.get();
    system("cls");
    int truefalse = 1;
    while (truefalse)
    {
    int choice = 0;
    cout << "Enter choice" << endl;
    cout << "1. Create a messagebox\n";
    cout << "2. Display a messagebox\n";
    cout << "3. More info on messageboxes\n";
    cout << "Choice: "; cin >> choice;
    if (choice > 3)
    {
  cout << "that is not a choice, choose a choice from 1-3\n";
    }
    else
    {
truefalse = 0;
    switch (choice) {
    case 1 : create();
    break;
    case 2 : display();
    break;
    case 3 : moreinfo();
    break;
    }
}
    }
    }