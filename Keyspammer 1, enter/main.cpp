#define _WIN32_WINNT 0x0500
#include <iostream>
#include <windows.h>


int main(){

while(1){

    int interval = 0; //Milisecond interval used in sleep()
    
    std::cout << "\nEnter miliseconds interval, or enter a non integer to quit 'q': ";

    //if typed in something bad, breakout of loop, program terminates.
    if ( ! (std::cin >> interval) ){
        break;
    }

    std::cout << "\nPress f11 to start and f12 to stop";

    //Setup input structures for function to emulate keyboard
    INPUT btndwn[2];

    ZeroMemory(btndwn, sizeof(btndwn));

    btndwn[0].type = INPUT_KEYBOARD;
    btndwn[0].ki.wVk =  0x31;
    btndwn[0].ki.wScan = MapVirtualKey(0x31, 0);
    btndwn[0].ki.dwFlags = KEYEVENTF_SCANCODE;

    btndwn[1].type = INPUT_KEYBOARD;
    btndwn[1].ki.wVk = VK_RETURN;
    btndwn[1].ki.wScan = MapVirtualKey(VK_RETURN, 0);
    btndwn[1].ki.dwFlags = KEYEVENTF_SCANCODE;

    //While the state of keyboard is NOT f11, loop doing nothing.
    while( !GetAsyncKeyState(VK_F11) ){
    
        //Do nothing, i.e. wait for input.    
    }
    
    //While state of keyboard is NOT F12, loop sending the keys.
    while( !GetAsyncKeyState(VK_F12) ){
        
        Sleep(interval); //Wait for a few miliseconds specified in interval
        SendInput(2, btndwn, sizeof(INPUT));    //Send keys specified in above structures.

    }

}
    return 0; //Main program exit returning success 0
}
